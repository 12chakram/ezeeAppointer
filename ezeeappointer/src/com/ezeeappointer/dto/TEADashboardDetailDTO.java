/**
 * 
 */
package com.ezeeappointer.dto;

/**
 * @author Administrator
 *
 */
public class TEADashboardDetailDTO {
	private String apptDate;

	/**
	 * @return the apptDate
	 */
	public String getApptDate() {
		return apptDate;
	}

	/**
	 * @param apptDate the apptDate to set
	 */
	public void setApptDate(String apptDate) {
		this.apptDate = apptDate;
	}
	
	
	
}
